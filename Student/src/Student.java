
public class Student
{
    int rollno;
    String name;
    static String collegeName = "SEGi University";
    
    public Student()
    {
    }
    
    public Student(String name, int rollno)
    {
        this.name = name;
        this.rollno = rollno;
    }
    
    public void display()
    {
        System.out.println(name + " " + rollno + " " + collegeName);
    }
    
    public static void main(String[] args)
    {
        // Student - Constructor usage
        Student s1 = new Student("Leela", 101);
        Student s2 = new Student();
        s1.display();
        s2.display();
        
        // Bike - Constructor call demo
        Bike bike = new Bike();
    }

}
