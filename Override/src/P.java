
public class P
{
    public void property()
    {
        System.out.println("Land + cash + assets");
    }
    
    public void marry()
    {
        System.out.println("Cute");
    }
    
    public static void main(String[] args)
    {
        P test1 = new P();
        test1.property();
        test1.marry();
        
        C test2 = new C();
        test2.property();
        test2.marry();
    }

}
