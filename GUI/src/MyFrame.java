import java.awt.*;

// Create custome Canvas for custome paint event
class MyCanvas extends Canvas
{
    @Override
    public void paint(Graphics g)
    {
        super.paint(g); // to avoid some possible error
        Font font = new Font("arial", Font.BOLD + Font.ITALIC, 14); // Define font
        this.setForeground(Color.CYAN);
        g.setFont(font);
        g.drawString("Hello World", 12, 12); // Draw string to canvas
    }
}

// Java GUI with AWT
public class MyFrame extends Frame
{
    public MyFrame()
    {
        this.setVisible(true);
        this.setTitle("Example for user defined");
        this.setSize(300, 300);
        this.setBackground(Color.BLUE);
        
        this.add(new MyCanvas()); // Add canvas to MyFrame
    }
}
