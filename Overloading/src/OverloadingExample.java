
public class OverloadingExample
{
    public static int add(int a, int b)
    {
        int result = a + b;
        System.out.println("a + b " + result);
        return result;
    }
    
    public static int add(int a, int b, int c)
    {
        int result = a + b + c;
        System.out.println("a + b + c " + result);
        return result;
    }
    
    public static void main(String[] args)
    {
        OverloadingExample adder = new OverloadingExample();
        adder.add(9, 10);
        adder.add(17, 18, 24);
    }

}
