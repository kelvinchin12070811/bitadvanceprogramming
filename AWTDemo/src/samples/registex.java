package samples;

import java.awt.Button;
import java.awt.Color;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

public class registex extends Frame implements ActionListener
{
    Frame f1 = null;
    Panel p1 = null;
    Label l1 = null, l2 = null;
    TextField t1 = null, t2 = null;
    Button b1 = null;

    registex()
    {
        setTitle("Example for Using GridLayout");
        setBounds(50, 50, 300, 300);
        setLayout(new GridLayout(1, 2));

        p1 = new Panel();
        p1.setBackground(Color.magenta);

        l1 = new Label("User Name");
        t1 = new TextField(20);
        l2 = new Label("Password");
        t2 = new TextField(20);
        b1 = new Button("Ok");
        b1.addActionListener(this);

        p1.add(l1);
        p1.add(t1);
        p1.add(l2);
        p1.add(t2);
        p1.add(b1);

        add(p1);
        t1.getText();
        t2.setEchoChar('*');

        setVisible(true);

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e)
            {
                dispose();
            }
        });
    }

    public void actionPerformed(ActionEvent ae)
    {
        if (ae.getSource() == b1)
        {
            if (t1.getText().equals("abc"))
            {
                f1 = new Frame("Already Registered");
                f1.setBounds(50, 50, 200, 200);
                f1.setBackground(Color.cyan);
                f1.setVisible(true);
                
                Label l3 = new Label("You are authenticated");
                l3.setForeground(Color.red);
                l3.setBounds(50, 50, 150, 150);
                f1.add(l3);
                f1.addWindowListener(new WindowAdapter() {
                    public void windowClosing(WindowEvent e)
                    {
                        f1.dispose();
                    }
                });
            }
            else
            {
                f1 = new Frame("Not Registered?");
                f1.setBounds(50, 50, 200, 200);
                f1.setBackground(Color.darkGray);
                f1.setVisible(true);
                Label l3 = new Label("You are not authenticated");
                l3.setForeground(Color.red);
                l3.setBounds(50, 50, 150, 150);
                f1.add(l3);
                f1.addWindowListener(new WindowAdapter() {
                    public void windowClosing(WindowEvent e)
                    {
                        f1.dispose();
                    }
                });
            }
        }
    }
    
    public static void main(String[] args)
    {
        registex r = new registex();
    }
}