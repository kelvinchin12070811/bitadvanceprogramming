import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;

public class AssetsLoader
{
    public static BufferedImage getCarTexture()
    {
        try
        {
            return ImageIO.read(AssetsLoader.class.getResource("/textures/Lawn_Mower.png"));
        }
        catch (IOException e)
        {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, e.getLocalizedMessage());
            System.exit(1);
        }
        return null;
    }
}
