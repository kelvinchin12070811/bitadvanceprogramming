import java.awt.Canvas;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class MainFrame extends JFrame
{
    private GridBagConstraints gbldef= null;
    private int height = 0;
    private int width = 0;
    private String title = null;
    
    // Components
    private Canvas[] canvases = null;
    
    private JLabel labCar1 = null;
    private JLabel labCar2 = null;
    private JLabel labCar3 = null;
    private JLabel labCar4 = null;
    
    private JTextField txfCar1 = null;
    private JTextField txfCar2 = null;
    private JTextField txfCar3 = null;
    private JTextField txfCar4 = null;
    
    public MainFrame(String title, int width, int height)
    {
        this.title = title;
        this.height = height;
        this.width = width;
        
        initFrame();
        initComponents();
        initLayout();
    }
    
    public Canvas[] getCanvases()
    {
        return canvases;
    }
    
    public JTextField[] getSpeedController()
    {
        JTextField[] rtnval = { txfCar1, txfCar2, txfCar3, txfCar4 };
        return rtnval;
    }
    
    private void initComponents()
    {
        canvases = new Canvas[4];
        for (int i = 0; i < canvases.length; i++)
            canvases[i] = new Canvas();
        
        labCar1 = new JLabel("Car 1: ");
        labCar2 = new JLabel("Car 2: ");
        labCar3 = new JLabel("Car 3: ");
        labCar4 = new JLabel("Car 4: ");
        
        txfCar1 = new JTextField("1");
        txfCar2 = new JTextField("1");
        txfCar3 = new JTextField("1");
        txfCar4 = new JTextField("1");
        
        int canvW = width;
        int canvH = 480 / 4;
        for (Canvas itr : canvases)
        {
            itr.setSize(canvW, canvH);
            itr.setMaximumSize(new Dimension(canvW, canvH));
            itr.setMinimumSize(new Dimension(canvW, canvH));
        }
    }
    
    private void initFrame()
    {
        this.setSize(width, height);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle(title);
    }
    
    private void initLayout()
    {
        this.setLayout(new GridBagLayout());
        gbldef = new GridBagConstraints();
        gbldef.insets = new Insets(3, 3, 3, 3);
        gbldef.fill = GridBagConstraints.BOTH;
        
        placeComponent(labCar1, 0, 0, 1, 1, 0.0, 0.0);
        placeComponent(txfCar1, 1, 0, 1, 1, 1.0, 0.0);
        placeComponent(labCar2, 2, 0, 1, 1, 0.0, 0.0);
        placeComponent(txfCar2, 3, 0, 1, 1, 1.0, 0.0);
        placeComponent(labCar3, 4, 0, 1, 1, 0.0, 0.0);
        placeComponent(txfCar3, 5, 0, 1, 1, 1.0, 0.0);
        placeComponent(labCar4, 6, 0, 1, 1, 0.0, 0.0);
        
        gbldef.insets.bottom = 6;
        placeComponent(txfCar4, 7, 0, 1, 1, 1.0, 0.0);
        
        gbldef.insets.top = 0;
        gbldef.insets.bottom = 0;
        for (int i = 0; i < canvases.length; i++)
        {
            if (i == canvases.length - 1)
                gbldef.insets.bottom = 3;
            
            placeComponent(canvases[i], 0, 1 + i, 8, 1, 1.0, 1.0);
        }
        
        
        this.pack();
    }
    
    private void placeComponent(Component comp,int gx, int gy, int gw, int gh, double wx, double wy)
    {
        gbldef.gridx = gx;
        gbldef.gridy = gy;
        gbldef.gridwidth = gw;
        gbldef.gridheight = gh;
        gbldef.weightx = wx;
        gbldef.weighty = wy;
        this.add(comp, gbldef);
    }
}
