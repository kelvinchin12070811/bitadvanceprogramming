
public class test2 extends test1
{
    @Override
    public void m3()
    {
        System.out.println("Method m3");
    }
    
    @Override
    public void m4()
    {
        System.out.println("Method m4");
    }
    
    public static void main(String[] args)
    {
        test2 obj = new test2();
        obj.m1();
        obj.m2();
        obj.m3();
        obj.m4();
    }
}
