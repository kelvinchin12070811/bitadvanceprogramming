
public class Cat {
	String colour = "";
	String foodPreference = "";
	int size = 0;
	int weight = 0;
	int mouseAmount = 0;
	int frequency = 0;
	
	public void catchMouse(int amount)
	{
		mouseAmount = amount;
	}
	
	public void eat(String food)
	{
		foodPreference = food;
	}
	
	public void miao(int tone)
	{
		frequency = tone;
	}
}
