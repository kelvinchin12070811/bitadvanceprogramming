
public class MyCat {

	public static void main(String[] args) {
		Cat kitty = new Cat();
		Cat garfield = new Cat();
		
		kitty.catchMouse(50);
		garfield.catchMouse(0);
		
		System.out.println("kitty " + kitty.mouseAmount);
		System.out.println("garfield " + garfield.mouseAmount);
	}
}
