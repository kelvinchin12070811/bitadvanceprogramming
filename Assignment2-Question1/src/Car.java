import java.awt.Graphics;
import java.awt.image.BufferedImage;

public class Car implements Drawable
{
    private static final double MAX_SPEED = 1000.0;
    
    private BufferedImage texture = null;
    private double x = 0;
    private double y = 0;
    private double maxWidth = 0;
    private double speed = 1.0;
    
    public Car(double x, double y, double maxWidth)
    {
        this.x = x;
        this.y = (y * (480 / 4)) + 15;
        this.maxWidth = maxWidth;
        
        texture = AssetsLoader.getCarImage();
    }
    
    public void setSpeed(double value)
    {
        speed = Math.abs(value);
    }

    @Override
    public void tick()
    {
        Director director = Director.getInstance();
        if (x > maxWidth)
            x = -texture.getWidth();
        else
            x += 1.0 * director.getDeltaTime() * (MAX_SPEED / speed);
    }

    @Override
    public void draw(Graphics g)
    {
        g.drawImage(texture, (int)x, (int)y, null);
    }
}
