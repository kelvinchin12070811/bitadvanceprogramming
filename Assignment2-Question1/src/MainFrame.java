import java.awt.Canvas;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class MainFrame extends JFrame
{
    private Canvas canvas = null;
    
    private JLabel labCar1 = null;
    private JLabel labCar2 = null;
    private JLabel labCar3 = null;
    private JLabel labCar4 = null;
    private JTextField txfCar1 = null;
    private JTextField txfCar2 = null;
    private JTextField txfCar3 = null;
    private JTextField txfCar4 = null;
    
    private int width = 0;
    private int height = 0;
    private String title = "";
    
    public MainFrame(String title, int width, int height)
    {
        this.title = title;
        this.width = width;
        this.height = height;
        
        initDisplay();
        initComponents();
        initLayout();
    }
    
    public Canvas getCanvas()
    {
        return canvas;
    }
    
    public JTextField[] getCarSpeedControllers()
    {
        JTextField[] rtnvle = { txfCar1, txfCar2, txfCar3, txfCar4 };
        return rtnvle;
    }
    
    private void initDisplay()
    {
        this.setSize(new Dimension(width, height));
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle(title);
        this.setLocationRelativeTo(null);
        this.setResizable(false);
    }
    
    private void initComponents()
    {
        canvas = new Canvas();
        
        labCar1 = new JLabel("Car 1:");
        labCar2 = new JLabel("Car 2:");
        labCar3 = new JLabel("Car 3:");
        labCar4 = new JLabel("Car 4:");
        
        txfCar1 = new JTextField("1");
        txfCar2 = new JTextField("1");
        txfCar3 = new JTextField("1");
        txfCar4 = new JTextField("1");
        
        canvas.setSize(width, height);
        canvas.setMaximumSize(new Dimension(width, height));
        canvas.setMinimumSize(new Dimension(width, height));
    }
    
    private void initLayout()
    {
        GridBagLayout layout = new GridBagLayout();
        this.setLayout(layout);
        GridBagConstraints gbcDef = new GridBagConstraints();
        gbcDef.insets = new Insets(3, 3, 3, 3);
        gbcDef.fill = GridBagConstraints.BOTH;
        
        this.add(labCar1, place(0, 0, 1, 1, 0, 0, gbcDef));
        this.add(txfCar1, place(1, 0, 1, 1, 1.0, 0, gbcDef));
        this.add(labCar2, place(2, 0, 1, 1, 0, 0, gbcDef));
        this.add(txfCar2, place(3, 0, 1, 1, 1.0, 0, gbcDef));
        this.add(labCar3, place(4, 0, 1, 1, 0, 0, gbcDef));
        this.add(txfCar3, place(5, 0, 1, 1, 1.0, 0, gbcDef));
        this.add(labCar4, place(6, 0, 1, 1, 0, 0, gbcDef));
        this.add(txfCar4, place(7, 0, 1, 1, 1.0, 0, gbcDef));
        this.add(canvas, place(0, 1, 8, 1, 1.0, 1.0, gbcDef));
        this.pack();
        
        gbcDef = null;
    }
    
    private GridBagConstraints place(int gx, int gy, int gw, int gh, double wx, double wy,
        GridBagConstraints gbcDef)
    {
        gbcDef.gridx = gx;
        gbcDef.gridy = gy;
        gbcDef.gridwidth = gw;
        gbcDef.gridheight = gh;
        gbcDef.weightx = wx;
        gbcDef.weighty = wy;
        return gbcDef;
    }
}
