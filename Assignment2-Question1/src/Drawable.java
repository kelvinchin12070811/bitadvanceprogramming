import java.awt.Graphics;

public interface Drawable
{
    public void tick();
    public void draw(Graphics g);
}
