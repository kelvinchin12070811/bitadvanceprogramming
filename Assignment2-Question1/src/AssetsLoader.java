import java.awt.image.BufferedImage;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;

public class AssetsLoader
{
    public static BufferedImage getCarImage()
    {
        try
        {
            return ImageIO.read(AssetsLoader.class.getResource("/textures/Lawn_Mower.png"));
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, e.getLocalizedMessage());
            System.exit(1);
        }
        return null;
    }
}
