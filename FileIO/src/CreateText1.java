import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class CreateText1
{
    public static void write() throws IOException
    {
        BufferedWriter fw = new BufferedWriter(new FileWriter(new File("mile.txt")));
        fw.write(100); //Unicode 'd'
        fw.newLine();
        fw.write("adele von ascham");
        fw.newLine();
        
        char[] chs = { 'a', 'b', 'c' };
        fw.write(chs);
        fw.flush();
        fw.close();
    }
    
    public static void read() throws IOException
    {
        FileReader fr = new FileReader(new File("mile.txt"));
        int ch = fr.read();
        while(ch != -1)
        {
            System.out.print((char)ch);
            ch = fr.read();
        }
        
        fr.close();
    }
    
    public static void readBuf() throws IOException
    {
        BufferedReader br = new BufferedReader(new FileReader("mile.txt"));
        String ln = br.readLine();
        while (ln != null)
        {
            System.out.println(ln);
            ln = br.readLine();
        }
        br.close();
    }
    
    public static void printWriter() throws IOException
    {
        PrintWriter pw = new PrintWriter(new File("rena.txt"));
        pw.println(15);
        pw.println(2);
        pw.println("rena");
        
        pw.flush();
        pw.close();
    }
    
    public static void main(String[] args) throws Exception
    {
        //write();
        //read();
        //readBuf();
        printWriter();
    }
}
