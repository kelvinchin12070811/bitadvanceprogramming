
public class Main
{
    public static void main(String[] args)
    {
        CIMBBank cimb = new CIMBBank();
        Maybank maybank = new Maybank();
        
        System.out.println("cimb: " + cimb.getInterestRate() + "%");
        System.out.println("maybank: " + maybank.getInterestRate() + "%");
    }
}
