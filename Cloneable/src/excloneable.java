import java.lang.Cloneable;

public class excloneable implements Cloneable
{
    int a = 10;
    int b = 20;
    
    public static void main(String[] args) throws Exception
    {
        excloneable obj = new excloneable();
        System.out.println(obj.a);
        System.out.println(obj.b);
        excloneable obj1 = (excloneable)obj.clone();
        obj1.a = 999;
        obj1.b = 888;
        System.out.println(obj1.a);
        System.out.println(obj1.b);
        System.out.println(obj.a);
        System.out.println(obj.b);
    }

}
